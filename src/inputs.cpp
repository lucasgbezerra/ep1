#include "inputs.hpp"

#include <string>
#include <iostream>

using namespace std;

string get_string()
{
    string valor;
    getline(cin, valor);
    return valor;
}


int get_int()
{
    while(true)
    {
        int valor;
        cin >> valor;
        if(cin.fail())
        {
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida! Insira novamente: " << endl;
        }
        else
        {
            cin.ignore(32767,'\n');
            return valor;
        }
    }
}
float get_float()
{
    while(true)
    {
        float valor;
        cin >> valor;
        if(cin.fail())
        {
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida! Insira novamente: " << endl;
        }
        else
        {
            cin.ignore(32767,'\n');
            return valor;
        }
    }
}

string valida_input()
{

    while(true)
    {
        string opcao;
        getline(cin, opcao);
    
        if (opcao == "S" || opcao == "s"||opcao == "N" || opcao == "n")
        {
            return opcao;
        }
        else
        {
            cout << "Entrada inválida! Insira novamente: ";
        }
    }
}
string valida_cpf()
{
   while(true)
    {
        int num_digitos =0;
        string cpf;
        getline(cin,cpf);
        for(char caractere: cpf)
        {
            if (isdigit(caractere))
            {
                num_digitos +=1;
            }
        }
        if(num_digitos == 11)
        {
            return cpf;
        }else
        {
            cout << "CPF inválido! Digite  11 digitos numericos: ";
        }
    }
}
string valida_nome()
{
   while(true)
    {
        string nome;
        getline(cin,nome);

        if(nome.length() != 0)
        {
            return nome;
        }else
        {
            cout << "Nome inválido! Nenhum nome foi digitado: ";   
        }
    }
}
string valida_telefone()
{
   while(true)
    {
        int num_digitos =0;
        string telefone;
        getline(cin,telefone);
        for(char caractere: telefone)
        {
            if (isdigit(caractere))
            {
                num_digitos +=1;
            }
        }
        if(num_digitos >= 10 && num_digitos <=11)
        {
            return telefone;
        }else
        {
            if (num_digitos < 10)
            {
               cout << "Telefone inválido! Digite APENAS NÚMEROS e no MÍNIMO 10 digitos: ";
            }else
            {
                cout << "Telefone inválido! Digite APENAS NÚMEROS e no MÁXIMO 11 digitos: ";
            }
        }
    }
}
string valida_email()
{
   while(true)
    {
        int num_caracteres =0;
        int num_pontuacao= 0;
        string email;
        getline(cin,email);
        for(char caractere: email)
        {
            if (isspace(caractere) || isupper(caractere))
            {
                num_caracteres +=1;
            }
            if(ispunct(caractere))
            {
                num_pontuacao+=1;
            }
        }
        if(num_caracteres == 0 && num_pontuacao >=2) 
        {
            return email;
        }else
        {
            cout << "Email invalido! Não corresponde todos requisitos [Ex:exemplo@gmail.com]: ";
        }
    }
}
