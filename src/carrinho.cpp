#include "carrinho.hpp"
#include "inputs.hpp"
#include "cliente.hpp"

#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <unistd.h>
#include <fstream>


using namespace std;

Carrinho::Carrinho()
{
}

Carrinho::~Carrinho()
{
}
void Carrinho::set_produto(Produto *lista_produtos)
{
    this->lista_produtos.push_back(lista_produtos);
}
vector<Produto *> Carrinho::get_produto()
{
    return lista_produtos;
}
void Carrinho::set_quantidade(int quantidade)
{
    this->quantidade.push_back(quantidade);
}
vector<int> Carrinho::get_quantidade()
{
    return quantidade;
}

void Carrinho::adicionar_produto(int codigo, int quantidade)
{
    Produto produto;
    string press_enter;
    string str_codigo = to_string(codigo);
    const string caminho_estoque = "arq/Estoque/" + str_codigo + ".txt";

    if (produto.verifica_codigo(codigo) == caminho_estoque)
    {
        set_produto(produto.arquivo_gera_produto(codigo));
        set_quantidade(quantidade);
    }
    
}
bool Carrinho::imprime_carrinho()
{

    vector<int> quantidade;
    quantidade = get_quantidade();
    int contador =0;
    vector<Produto *>lista_carrinho = get_produto(); 
    string press_enter;

    system("clear");
    if(lista_carrinho.size() != 0)
    {
        cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<< endl;
        cout << "                           CARRINHO                              "<<endl;
        cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<< endl;
        for(Produto *p:get_produto())
        {
            cout <<"CÓDIGO: "<< p ->get_codigo() << " | ";
            cout <<"NOME: " << p ->get_nome_produto()<< " | ";
            cout <<"PREÇO: R$" << p ->get_preco()<< " | ";
            cout <<"QUANTIDADE: "<< quantidade[contador] << endl;
            contador+=1;
        }
        cout << "-----------------------------------------------------------------"<< endl;

        if(compara_quantidades()==true)
        {
            return true;
    
        }else
        {
            cout << "-----------------------------------------------------------------"<< endl;
            cout << "QUANTIDADE DESEJADA NÃO ENCONTRADA EM ESTOQUE" << endl;
            cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<< endl;
            cout << "Para sair presssione ENTER" << endl;
            press_enter = get_string();
            return false;
        }
        
    }else
    {
        cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<< endl;
        cout << "------------------------ CARRINHO VAZIO--------------------------" << endl;
        cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<< endl;
        cout << "Para sair presssione ENTER" << endl;
        press_enter = get_string();
        return false;
    }
    
}
bool Carrinho::compara_quantidades()
{
    bool quantidade_suficiente;

    for(int q:get_quantidade())
    {
        for(Produto *p:get_produto())
        {
            if(q > p->get_quantidade())
            {
                quantidade_suficiente = false;
            }else
            {
                quantidade_suficiente= true;
            } 
        }
    }   

    return quantidade_suficiente;
}

float Carrinho::calcula_preco(float desconto)
{
    vector<int> lista_quantidade = get_quantidade() ;
    float preco;
    float total =0.0;
    int contador =0;


    for(Produto *p: get_produto())
    {
        preco = p->get_preco();
        total = total + preco*lista_quantidade[contador];
        contador+=1;    
    }
    return total*(1-desconto);
}

void Carrinho::finalizar_compra(string cpf)
{
    string finalizar;    
    fstream arquivo;
    vector<int>lista_quantidade = get_quantidade();
    int contador =0;
    string str_codigo;


    cout << "Para finalizar presssione ENTER" << endl;
    finalizar = get_string();
    registrar_recomendacao(cpf);

    for (Produto *p: get_produto())
    {
        p ->set_quantidade(p->get_quantidade()-lista_quantidade[contador]);
        contador+=1;
    }
    for(Produto *p: get_produto())
    {
        str_codigo = to_string(p ->get_codigo());
        arquivo.open("arq/Estoque/"+str_codigo+ ".txt",fstream::out);

        if (arquivo.is_open())
        {
            arquivo << p ->get_codigo()<< "\n";
            arquivo << p ->get_nome_produto() << "\n";
            arquivo << p ->get_quantidade()<< "\n";
            arquivo << p ->get_preco()<< "\n";
            arquivo << p ->get_categoria()<< "\n";
            arquivo.close();
        }
    }

}
void Carrinho::registrar_recomendacao(string cpf)
{
    fstream arquivo;
    vector<string> lista_dados_cliente;
    string linha_arquivo;
    int size_lista;
    int size_dados_cliente;
    vector <string> lista_categoria_quantidade;
    vector <string> lista_categorias;
    vector <vector <string>> lista_das_listas;
    vector<int>lista_quantidade = get_quantidade();
    int ind =0;
    int quantidade_aux;


    arquivo.open("arq/Cadastros_clientes/"+ cpf +".txt",fstream::in);
    if (arquivo.is_open())
    {
        while (getline(arquivo,linha_arquivo))
        {
            lista_dados_cliente.push_back(linha_arquivo);
        }
        arquivo.close();
    }else
    {
        cout << "Erro ao tentar encontra cadastro do cliente" << endl;
    }
    for (Produto *p:get_produto())
    {
        lista_categorias.push_back(p ->get_categoria());
    }
    sort(lista_categorias.begin(),lista_categorias.end());
    size_lista = lista_categorias.size();
    for (int ind =0;ind <size_lista;ind++)
    {
        for (int cont =0; cont<size_lista ;cont++)
        {
            if (lista_categorias[ind]== lista_categorias[cont] && cont != ind)
            {
                lista_categorias.erase(lista_categorias.begin()+ind);
                lista_quantidade[cont]+=lista_quantidade[ind];
                lista_quantidade.erase(lista_quantidade.begin()+ind);
            }
        }  
    }
    size_dados_cliente= lista_dados_cliente.size();
    for (Produto *p:get_produto())
    {
       lista_categoria_quantidade.push_back( p ->get_categoria());
       lista_categoria_quantidade.push_back(to_string(lista_quantidade[ind]));
       lista_das_listas.push_back(lista_categoria_quantidade);
       ind+=1;
       lista_categoria_quantidade.clear();
    }
       
    if(size_dados_cliente > 5)
    {
        size_lista = lista_categorias.size();
        arquivo.open("arq/Cadastros_clientes/"+cpf+".txt",fstream::out);
        if(arquivo.is_open())
        {
            arquivo << lista_dados_cliente[0] << "\n";
            arquivo << lista_dados_cliente[1] << "\n";
            arquivo << lista_dados_cliente[2] << "\n";
            arquivo << lista_dados_cliente[3] << "\n";
            arquivo << lista_dados_cliente[4] << "\n";
            for(int ind =5;ind < size_dados_cliente-1;ind+=2)
            {
                for (int cont= 0; cont < size_lista;cont++)
                {
                    if (lista_dados_cliente[ind]==lista_categorias[cont])
                    {
                        quantidade_aux = stoi(lista_dados_cliente[ind+1]) + lista_quantidade[cont];
                        lista_dados_cliente[ind+1]= to_string(quantidade_aux);
                        lista_categorias.erase(lista_categorias.begin()+cont);
                        lista_quantidade.erase(lista_quantidade.begin()+cont);
                        size_lista = lista_categorias.size();
                    }
                }
                
            }
            if (size_lista != 0)
            {
                for(int ind =5;ind < size_dados_cliente;ind++)
                {
                    arquivo << lista_dados_cliente[ind] << "\n";   
                }
                for(int ind =0;ind < size_lista ; ind++)
                {
                    arquivo<< lista_categorias[ind]<< "\n";
                    arquivo<< lista_quantidade[ind]<< "\n";            
                }
            }else
            {
                for(int ind =5;ind < size_dados_cliente;ind++)
                {
                    arquivo << lista_dados_cliente[ind] << "\n";   
                }
            }
            
        }
        arquivo.close();
        
    }else
    {
        arquivo.open("arq/Cadastros_clientes/"+cpf+".txt",fstream::app);
        if(arquivo.is_open())
        {
            if (lista_categorias.size() == lista_quantidade.size())
            {
                size_lista = lista_quantidade.size();
                for(int ind =0;ind < size_lista ; ind++)
                {
                    arquivo<< lista_categorias[ind]<< "\n";
                    arquivo<< lista_quantidade[ind]<< "\n";            
                }
            }
        
        }
        arquivo.close();
    }    
}
