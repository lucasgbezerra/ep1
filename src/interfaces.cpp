#include "interfaces.hpp"
#include "inputs.hpp"
#include "cliente.hpp"
#include "produto.hpp"
#include "carrinho.hpp"
#include "socio.hpp"

#include <unistd.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>


#define PATH_ESTOQUE "arq/Estoque/estoque.txt"

using namespace std;

Interfaces::Interfaces()
{
}
Interfaces::~Interfaces()
{
}
void Interfaces::menu_pricipal()
{
    system("clear");
    cout << "-------------------------------------"<<endl;
    cout << "   (1) Modo de venda " << endl;
    cout << "   (2) Modo recomendação  " << endl;
    cout << "   (3) Modo estoque  " << endl;
    cout << "   (0) Sair  " << endl;
    cout << "-------------------------------------"<<endl;
    cout << "Escolha o modo de operação: ";
}
void Interfaces::modo_venda()
{

    system("clear");
    cout << "-------------------------------------"<<endl;
    cout << "           Modo Venda                " << endl;
    cout << "-------------------------------------"<<endl;  
    modo_venda_cadastro();  


}

void Interfaces::modo_venda_cadastro()
{
    string CPF;
    string nome;
    string email;
    string telefone;
    string opcao_socio;
    bool socio;
    fstream arquivo;
    Cliente *cliente;

    
    cout << "Digite o CPF do cliente [Somente números]: ";
    CPF = valida_cpf();
    
    arquivo.open("arq/Cadastros_clientes/" + CPF+ ".txt");
    if (arquivo.fail())
    {
        arquivo.close();
        cout << "O cliente ainda não possui cadastro, Cadastre-o" << endl;
        cout << "Digite o nome do cliente: ";
        nome = valida_nome();
        cout << "Digite o email do cliente: ";
        email = valida_email();
        cout << "Digite o telefone do cliente com DDD [Somente números]: ";
        telefone = valida_telefone();
        cout << "O cliente deseja se tornar socio [S/N]: ";
        opcao_socio = valida_input();
             
        if(opcao_socio == "S" || opcao_socio == "s")
        {
            socio = true;
        }else
        {
            socio = false;            
        }
        
        if(socio == true)
        {
            cliente = new Socio(CPF,nome,telefone,email);
        }else
        {
            cliente = new Cliente(CPF,nome,telefone,email);
        }
        cliente->cliente_gera_arquivo();
        imprime_estoque();
        cliente ->preenche_carrinho();
        cliente ->imprime_gastos();

    }else
    {                
        arquivo.close();
        modo_recomendacao();

    }
}

void Interfaces::imprime_estoque()
{
    fstream arquivo;
    string linha_arquivo;
    vector<string>lista_estoque;
    int size;

    arquivo.open(PATH_ESTOQUE,fstream::in);                
    if (arquivo.is_open())
    {
        while (getline(arquivo,linha_arquivo))
        {
            lista_estoque.push_back(linha_arquivo);
        }
        arquivo.close();
    }
    system("clear");
    size = lista_estoque.size();
    for (int ind =0;ind<size;ind++)
    {  
        if(ind%2 ==0)
        {
            cout << "("<<lista_estoque[ind]<<") "<< lista_estoque[ind+1]<< endl;
        }
    }
    cout << "-------------------------------------------" << endl;
    cout <<"(0) " << "FINALIZAR COMPRA" << endl;
    cout << "-------------------------------------------" << endl;
}
void Interfaces::modo_recomendacao()
{
    string cpf;
    fstream arquivo;
    Cliente *cliente;
    string linha_arquivo;
    vector <string>lista_dados_cliente;
    string resposta;

    system("clear");
    cout << "-------------------------------------"<<endl;
    cout << "           Modo Recomendação           " << endl;
    cout << "-------------------------------------"<<endl;

    cout << "Digite o CPF do cliente [Somente números]: ";
    cpf = valida_cpf();
    arquivo.open("arq/Cadastros_clientes/" + cpf+ ".txt");
    if (arquivo.fail())
    {
        arquivo.close();
        cout << "*********O cliente ainda não possui cadastro*********" << endl;
        sleep(2);
    }else
    {
        arquivo.close();
        arquivo.open("arq/Cadastros_clientes/"+ cpf+ ".txt",fstream::in);
        if (arquivo.is_open())
        {
            while (getline(arquivo,linha_arquivo))
            {
                lista_dados_cliente.push_back(linha_arquivo);
            }
            arquivo.close();
        }
        
        if(lista_dados_cliente[4] == "1")
        {
            cliente = new Socio(lista_dados_cliente);            
        }else
        {
            cliente = new Cliente(lista_dados_cliente);
        }
        cout << "Dados do cliente:" << endl;
        cout <<"CPF: " << cliente ->get_cpf() << endl;
        cout <<"NOME: " << cliente ->get_nome() << endl;
        cout <<"EMAIL: " << cliente ->get_email() << endl;
        cout <<"TELEFONE: " << cliente ->get_telefone() << endl;
        if (cliente -> get_socio()== true)
        {
            cout<< "SÓCIO: "<< "sim" << endl;
        }else
        {
            cout<< "SÓCIO: "<< "não" << endl;
        }
        sleep(2);
        cliente ->imprime_recomendacao();
        cout << "Mostrar produtos alem dos recomendados [S/N]: ";
        resposta = valida_input();
        if (resposta == "S"||resposta == "s" )
        {
            imprime_estoque();
        }
        cliente ->preenche_carrinho();
        cliente ->imprime_gastos();

    }
    
}
void Interfaces::modo_estoque_menu()
{
    int opcao_estoque;
    
    system("clear");
    cout << "*------------------------------------*" << endl;
    cout << "           Modo Estoque               " << endl;
    cout << "*------------------------------------*" << endl;
    cout << "   (1) Cadastrar novo produto " << endl;
    cout << "   (2) Adicionar categoria  " << endl;
    cout << "   (3) Adicionar quantidade de produto"<<endl;
    cout << "   (4) Imprimir informações do produto"<<endl;
    cout << "   (0) Sair  " << endl;
    cout << "-------------------------------------"<<endl;
    cout << "Escolha o modo de operação: ";
    opcao_estoque = get_int();
    switch (opcao_estoque)
    {
    case 1:
        cadastrar_produto();
        break;
    case 2:
        adicionar_categoria();
        break;
    case 3:
        adicionar_quantidade();
        break;
    case 4:
        imprimir_informacao_produto();
        break;
    case 0:
        break;
    default:
        cout << "Opção invalida!"<< endl;
        break;
    }
}
void Interfaces::cadastrar_produto()
{
    string nome;
    int quantidade;
    int codigo;
    string str_codigo;
    string categoria;
    float preco;
    fstream arquivo;

    cout << "Digite o nome do novo produto: ";
    nome = get_string();
    cout << "Digite a categoria do novo produto: " ;
    categoria = get_string();
    transform(categoria.begin(),categoria.end(),categoria.begin(),::toupper);
    cout << "Digite o codigo do novo produto: " ;
    codigo = get_int();
    str_codigo = to_string(codigo);
    arquivo.open("arq/Categorias/"+categoria+".txt");
    if (arquivo.fail())
    {
        arquivo.close();
        cout << "****Esta categoria não esta cadastrada. Cadastre-a****"<< endl;
    }
    arquivo.close();
    cout << "Digite o preço do novo produto: " ;
    preco = get_float();
    cout << "Digite a quantidade do novo produto: ";
    quantidade = get_int();
    arquivo.open("arq/Estoque/"+str_codigo+".txt");
    if (arquivo.is_open())
    {
        cout << "****Um produto já esta cadastrado com esse codigo****" << endl;
        sleep(1);
    }else
    {
        arquivo.close();
        arquivo.open("arq/Estoque/"+str_codigo+".txt",fstream::out);
        if(arquivo.is_open())
        {
            arquivo << codigo << "\n";
            arquivo << nome << "\n";
            arquivo << quantidade << "\n";
            arquivo << preco << "\n";
            arquivo << categoria << "\n";
            arquivo.close();
        }
        arquivo.open(PATH_ESTOQUE,fstream::app);
        if(arquivo.is_open())
        {
            arquivo << codigo << "\n";
            arquivo << nome << "\n";            
        }
        arquivo.close();
        arquivo.open("arq/Categorias/"+categoria+".txt");
        if (arquivo.is_open())
        {
            arquivo.close();
            arquivo.open("arq/Categorias/"+categoria+".txt",fstream::app);
            if(arquivo.is_open())
            {
                arquivo << codigo << "\n";
                arquivo << nome << "\n";            
            }
           arquivo.close();
        }
    }
    arquivo.close();
    
}
void Interfaces::adicionar_categoria()
{
    string linha_arquivo;
    vector<string> dados_produto;
    string codigo;
    vector <string> codigo_lista;
    string categoria;
    fstream arquivo;
    string resposta;

    imprime_estoque();
    cout << "Digite o nome da nova categoria,[Ex: CATEGORIA]: ";
    categoria = get_string();
    transform(categoria.begin(),categoria.end(),categoria.begin(),::toupper);
    arquivo.open("arq/Categorias/"+categoria+".txt");
    if (arquivo.is_open())
    {
        cout << "Esta categoria ja esta cadastrada"<< endl;
        sleep(1);
    }else
    {
        arquivo.close();
        do
        {
            cout << "Digite o codigo do produto que pertence a ela: ";
            codigo = get_string();
            arquivo.open("arq/Estoque/"+codigo+".txt");
            if(arquivo.fail())
            {
                cout << "****Não existe produto com esse codigo****" <<endl;
            }                
            arquivo.close();
            codigo_lista.push_back(codigo);
            cout << "gostaria de adcionar mais algum produto [S/N]: ";
            resposta = valida_input();    
        } while (resposta == "s" ||resposta == "S");
        for(string cod:codigo_lista)
        {
            arquivo.open("arq/Estoque/"+cod+".txt",fstream::in);                
            if (arquivo.is_open())
            {
                while (getline(arquivo,linha_arquivo))
                {
                    dados_produto.push_back(linha_arquivo);
                }
                arquivo.close();
                arquivo.open("arq/Categorias/"+categoria+".txt",fstream::app);
                if(arquivo.is_open())
                {
                    arquivo << dados_produto[0] << "\n";
                    arquivo << dados_produto[1] << "\n";
                }
                arquivo.close();
                dados_produto.clear();
            }
            
        }
    }
       
}

void Interfaces::adicionar_quantidade()
{
    
    string linha_arquivo;
    vector<string> dados_produto;
    string codigo;
    int quantidade;
    int nova_quantidade;
    fstream arquivo;
    
    imprime_estoque();
    cout << "Digite o codigo do produto que dejesa alterar: ";
    codigo = get_string();
    cout << "Digite a quantidade a ser adicionada do produto: ";
    quantidade = get_int();
    arquivo.open("arq/Estoque/"+codigo+".txt");
    if (arquivo.fail())
    {
        cout << "Este produto não esta cadastrado"<< endl;;
    }else
    {
        arquivo.close();
        arquivo.open("arq/Estoque/"+codigo+".txt",fstream::in);                
        if (arquivo.is_open())
        {
            while (getline(arquivo,linha_arquivo))
            {
                dados_produto.push_back(linha_arquivo);
            }
            arquivo.close();
        }
        nova_quantidade = stoi(dados_produto[2])+ quantidade;
        arquivo.open("arq/Estoque/"+codigo+".txt",fstream::out);
        if (arquivo.is_open())
        {
            arquivo << dados_produto[0] << "\n";
            arquivo << dados_produto[1] << "\n";
            arquivo << nova_quantidade << "\n";
            arquivo << dados_produto[3] << "\n";
            arquivo << dados_produto[4] << "\n";
            arquivo.close();
        }
    }

}

void Interfaces::imprimir_informacao_produto()
{
    string codigo;
    fstream arquivo;
    string linha_arquivo;
    vector<string> dados_produto;
    string press_enter;

    imprime_estoque();
    cout << "Digite o codigo do produto que dejesa alterar: ";
    codigo = get_string();
    arquivo.open("arq/Estoque/"+codigo+".txt");
    if (arquivo.fail())
    {
        cout << "Este produto não esta cadastrado"<< endl;;
    }else
    {
        arquivo.close();
        arquivo.open("arq/Estoque/"+codigo+".txt",fstream::in);                
        if (arquivo.is_open())
        {
            while (getline(arquivo,linha_arquivo))
            {
                dados_produto.push_back(linha_arquivo);
            }
            arquivo.close();
        }
        system("clear");
        cout << "Dados do Produto: " << endl;
        cout << "---------------------------------" << endl;
        cout << "NOME: " << dados_produto[1] <<endl;
        cout << "CÓDIGO: " << dados_produto[0] <<endl;
        cout << "QUANTIDADE: " << dados_produto[2] << endl;
        cout << "PREÇO: R$" << dados_produto[3] <<endl;
        cout << "CATEGORIA: " << dados_produto[4] <<endl;
        cout << "---------------------------------" << endl;
        cout << "Pressione ENTER para voltar ao menu principal" << endl;
        press_enter = get_string(); 

    }
}
