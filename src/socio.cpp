#include "socio.hpp"
#include <iostream>

Socio::Socio()
{
}

Socio::Socio(string cpf, string nome,string telefone,string email)
{
    set_cpf(cpf);
    set_nome(nome);
    set_email(email);
    set_telefone(telefone);
    set_socio(true);
}
Socio::Socio(vector<string> lista_dados)
{
       
    set_cpf(lista_dados[0]);
    set_nome(lista_dados[1]);
    set_email(lista_dados[2]);
    set_telefone(lista_dados[3]);
    set_socio(true);
}
Socio::~Socio()
{
}
void Socio::set_desconto()
{
    this->desconto = 0.15;
}
float Socio::get_desconto()
{
    return desconto;
}


void Socio::imprime_gastos()
{
    float total;
    float total_descontado;
    bool imprime_total;
    imprime_total = get_carrinho().imprime_carrinho();
    if (imprime_total == true)
    {
        total_descontado = get_carrinho().calcula_preco(0.15);
        total= get_carrinho().calcula_preco(0);
        cout << "VALOR TOTAL DOS PRODUTOS: R$"<< total << endl;
        cout << "DESCONTO OFERECIDO: R$"<< total-total_descontado << endl;
        cout << "VALOR FINAL DA VENDA: R$"<< total_descontado << endl;
        cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<< endl;
        get_carrinho().finalizar_compra(get_cpf());
    }


}