#include "cliente.hpp"
#include "inputs.hpp"

#include<algorithm>
#include<functional>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

Cliente::Cliente()
{  
}
Cliente::~Cliente()
{  
}
Cliente::Cliente(vector<string> lista_dados)
{
       
    set_cpf(lista_dados[0]);
    set_nome(lista_dados[1]);
    set_email(lista_dados[2]);
    set_telefone(lista_dados[3]);
    set_socio(false);
}
Cliente::Cliente(string cpf, string nome,string telefone,string email)
{
    set_cpf(cpf);
    set_nome(nome);
    set_email(email);
    set_telefone(telefone);
    set_socio(socio);
}

string Cliente::get_cpf()
{
    return cpf;
}

void Cliente::set_cpf(string cpf)
{
    this->cpf = cpf;

}
string Cliente::get_nome()
{
    return nome;
}
void Cliente::set_nome(string nome)
{
    this->nome = nome;
}
string Cliente::get_email()
{
    return email;
}
void Cliente::set_email(string email)
{
    this->email = email;
}
bool Cliente::get_socio()
{
    return socio;    
}
void Cliente::set_socio(bool socio)
{
    this->socio = socio;
}

string Cliente::get_telefone()
{
    return telefone;
}
void Cliente::set_telefone(string telefone)
{
    this->telefone = telefone;
}
Carrinho Cliente::get_carrinho()
{
    return carrinho;

}

void Cliente::cliente_gera_arquivo()
{
    fstream arquivo;
    arquivo.open("arq/Cadastros_clientes/" + get_cpf() + ".txt", fstream::app|fstream::out); 
    if(arquivo.is_open())
    {
        arquivo << get_cpf() << "\n";
        arquivo << get_nome() << "\n";
        arquivo << get_email() << "\n";
        arquivo << get_telefone() << "\n";
        arquivo << get_socio() << "\n";
        arquivo.close();
    }
}


void Cliente::preenche_carrinho()
{
    vector<int> lista_codigo;
    int codigo = -1;
    vector<int> lista_quantidade;
    int quantidade;
    int size_lista;
    Produto *produto = new Produto();
    string caminho_estoque;


    while (codigo != 0)
    {
        cout << "Digite o codigo do produto desejado: ";
        codigo = get_int();
        if (codigo == 0)
        {
            break;
        }else
        {
            caminho_estoque = produto->verifica_codigo(codigo);
            if ("arq/Estoque/"+to_string(codigo)+".txt"==caminho_estoque)
            {
                cout << "Digite a quantidade desejada: ";
                quantidade = get_int();
                lista_quantidade.push_back(quantidade);
                lista_codigo.push_back(codigo);
            }else
            {
                cout <<"Esse produto não esta no estoque"<<endl;
            }
            

        }  
    }
    size_lista = lista_codigo.size();
    if(size_lista > 1)
    {
        for(int ind =0;ind < size_lista;ind++)
        {
            for(int cont = 0;cont < size_lista ;cont++)
            {
                if (lista_codigo[ind]==lista_codigo[cont] && ind != cont)
                {
                    lista_quantidade[ind] += lista_quantidade[cont];
                    lista_codigo.erase(lista_codigo.begin()+cont);
                    lista_quantidade.erase(lista_quantidade.begin()+cont);
                    size_lista -=1;
                }
            }
        }
    }
    for (int cont =0; cont < size_lista;cont++)
    {
        carrinho.adicionar_produto(lista_codigo[cont],lista_quantidade[cont]);
    }

}
void Cliente::imprime_gastos()
{
    float total;
    float total_descontado;
    bool imprime_total;
    imprime_total = carrinho.imprime_carrinho();
    
    if (imprime_total == true)
    {
        total =carrinho.calcula_preco(0.0);
        total_descontado =carrinho.calcula_preco(0.0);

        cout << "VALOR TOTAL DOS PRODUTOS: R$"<< total << endl;
        cout << "DESCONTO OFERECIDO: R$"<< total-total_descontado << endl;
        cout << "VALOR FINAL DA VENDA: R$"<< total_descontado << endl;
        cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<< endl;

        carrinho.finalizar_compra(get_cpf());
    }
    
}
void Cliente::imprime_recomendacao()
{
    fstream arquivo;
    vector <string> lista_dados_cliente;
    string linha_arquivo;
    vector <int> dados_quantidade;
    vector<string> lista_recomendacao;
    vector<string> dados_categoria;
    int contador=0;
    int indice =0;
    int size_lista;
    vector<int> categoria_size;
 
    arquivo.open("arq/Cadastros_clientes/"+get_cpf()+".txt", fstream::in);
     if (arquivo.is_open())
    {
        while (getline(arquivo,linha_arquivo))
        {
            lista_dados_cliente.push_back(linha_arquivo);
        }
        arquivo.close();
        size_lista =lista_dados_cliente.size();
        if(size_lista < 6)
        {
            cout << "-----------------------------------------------" << endl;
            cout << "       Cliente sem histórico de compras" << endl;
            cout << "-----------------------------------------------" << endl;
        }else
        {
            for (int ind=6;ind <size_lista ; ind+=2)
            {
                dados_quantidade.push_back(stoi(lista_dados_cliente[ind]));
            }
            sort(dados_quantidade.begin(),dados_quantidade.end(),greater<int>());
            for (int c:dados_quantidade)
            {
                for (int ind=6;ind < size_lista; ind+=2)
                {
                    if(c == stoi(lista_dados_cliente[ind]))
                    {

                        lista_recomendacao.push_back(lista_dados_cliente[ind -1]);
                    }
                }  
            }
            size_lista = lista_recomendacao.size();
            for (int cont = 0; cont < size_lista;cont++)
            {
                for (int ind = 0; ind < size_lista; ind++)
                {
                    if (lista_recomendacao[cont]== lista_recomendacao[ind] && cont != ind)
                    {
                        lista_recomendacao.erase(lista_recomendacao.begin()+ind);
                        size_lista = lista_recomendacao.size();
                    }
                }  
            }

            for (string cat:lista_recomendacao)
            {
                    categoria_size.push_back(dados_categoria.size());
                    arquivo.open("arq/Categorias/"+cat+".txt",fstream::in);
                    while (getline(arquivo,linha_arquivo))
                    {
                        dados_categoria.push_back(linha_arquivo);
                    }
                    arquivo.close();
            }
            size_lista = dados_categoria.size();
            system("clear");
            cout << "********Produtos recomendados para " << get_nome() <<"*******" <<endl;
            contador =0;
            do
            {
                if (contador == categoria_size[indice])
                {
                        cout <<"-----------"<< lista_recomendacao[indice]<<"-----------" << endl;
                        indice +=1;
                }                
                if(contador < size_lista){
                    cout << "(" <<dados_categoria[contador] <<") " << dados_categoria[contador+1] << endl;
                }else
                {
                    break;
                }
                contador+=2;                    
            } while (contador/2 <=10);
            cout << "---------------------------------------" <<endl;
            cout << "(0) FINALIZAR" <<endl;
            cout << "---------------------------------------" <<endl;
        }
    }else
    {
        cout << "Erro ao tentar encontrar cadastro do cliente" << endl;
    }
    arquivo.close();
    
}