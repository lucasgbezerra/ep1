#include "interfaces.hpp"
#include "inputs.hpp"
#include "cliente.hpp"
#include "produto.hpp"
#include "carrinho.hpp"

#include <iostream>

using namespace std;

int main()
{

    int modo_operacao =-1;
    Interfaces menu;

    while(modo_operacao != 0)
    {
        
        menu.menu_pricipal();
        modo_operacao = get_int();
        
        switch (modo_operacao)
        {
            case 1:
                menu.modo_venda();
                break;                
            case 2: 
                menu.modo_recomendacao();
                break;
            case 3:
               menu.modo_estoque_menu();
               break;
            case 0:
                break;                         
            default:
                break;
        }
        
    }

    return 0;
}

