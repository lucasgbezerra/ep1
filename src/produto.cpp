#include "produto.hpp"
#include "inputs.hpp"
#include "interfaces.hpp"


#include <fstream>
#include <string>
#include <iostream>
#include <vector>

using namespace std;

Produto::Produto()
{
}
Produto::Produto(int codigo,string nome_produto,int quantidade,float preco,string categoria)
{
    set_codigo(codigo);
    set_nome_produto(nome_produto);
    set_categoria(categoria);
    set_preco(preco);
    set_quantidade(quantidade);
}
Produto::~Produto()
{    
}
int Produto::get_codigo()
{
    return codigo;
}
void Produto::set_codigo(int codigo)
{
    this->codigo = codigo;
}
string Produto::get_nome_produto()
{
    return nome_produto;
}
void Produto::set_nome_produto(string nome_produto)
{
    this->nome_produto = nome_produto;
}
float Produto::get_preco()
{
    return preco;
}
void Produto::set_preco(float preco)
{
    this->preco = preco;
}

string Produto::get_categoria()
{
    return categoria;
}
void Produto::set_categoria(string categoria)
{
    this->categoria = categoria;
}
int Produto::get_quantidade()
{
    return quantidade;
}

void Produto::set_quantidade(int quantidade)
{
    this->quantidade = quantidade;
}

string Produto::verifica_codigo(int codigo)
{
    fstream arquivo;
    string str_codigo = to_string(codigo);

    arquivo.open("arq/Estoque/"+ str_codigo + ".txt",fstream::in);                
    if (arquivo.is_open())
    {
        return "arq/Estoque/"+str_codigo+".txt";
    }else
    {
        return "Esse produto não esta no estoque";
    }
    
}

Produto * Produto::arquivo_gera_produto(int codigo)
{
    fstream arquivo;
    Produto *produto;
    string str_codigo = to_string(codigo);
    string linha_arquivo;
    vector <string> produto_dados;

    if (verifica_codigo(codigo) =="arq/Estoque/"+str_codigo+".txt")
    {
        arquivo.open("arq/Estoque/"+ str_codigo + ".txt",fstream::in);                
        if (arquivo.is_open())
        {
            while (getline(arquivo,linha_arquivo))
            {
                produto_dados.push_back(linha_arquivo);
            }
            produto = new Produto(codigo,produto_dados[1],stoi(produto_dados[2]),stof(produto_dados[3]),produto_dados[4]);   
        }
        arquivo.close();
        
    }else
    {
        cout << "Produto não encontrado"<< endl;
    }
    
    return produto;

}
