# EP1 - OO 2019.2 (UnB - Gama)

## Descrição

Programa para o gerenciamento de uma loja de venda de produtos. 
- Funcionalidades:

    - **Cadastrar produtos**
    - **Cadastrar clientes**
    - **Fazer Vendas**
    - **Calcular valor da venda**
    - **Recomendar produtos**

## Como utillizar

Primeiramente, faça um clone da pasta ep1. Após clonar a pasta acesse-a,pelo terminal, no seu computador e execute os seguintes comandos:
    
    -make clean
    -make
    -make run

Após executar o commandos O programa começará a ser executado e será mostrado na tela três modos.
    
-**Modo venda**: Modo responsável por fazer vendas. Será pedido o CPF do cliente. Caso ele não tenha cadastros será pedido dados pra o cadastro, caso contrário ele será redirecionado para o modo recomendação.Após o cadastro o estoque será mostrado e será possível selecionar, por meio do código, os produtos que serão vendidos para o cliente. Depois que a opção finalizae for inserida o carrinho será mostrado junto com os calcúlos de preço e Desconto.

-**Modo Recomendação** : Esse modo só funcionará caso o cliente já possua cadastro. Após a inserção do CPF o cadastro do cliente será mostrado. Subsequentemente, será exibido produtos recomendados para o cliente baseado no histórico de compras. Caso o cliente queira comprar algo que não foi recomendado confirme a mensagem e o estoque será mostrado na tela. Selecione, por meio do código, os produtos que serão vendidos para o cliente. Depois que a opção finalizar for inserida o carrinho será mostrado junto com os calcúlos de preço e Desconto.

-**Modo Estoque**: Modo responsável por cadastrar produtos, categorias e adicionar novas quantidades a produtos já cadastrado. Menu do modo estoque conterá:
- Menu do modo estoque conterá:

    - **Cadastrar novo produto**: Cadastre novos produtos no estoque da loja digitando os dados pedidos. Códigos iguais a de produtos já cadastrados não serão aceitos.
    
    - **Cadastrar clientes**: Adicionar categoria, cria nova categoria e adciona produtos nela produtos nela.

    - **Adiciona quantidade de produtos**: atualiza o número de produtos em estoque.

    - **Imprimir informações do produto**: Mostra todas as informações de produtos cadastrados.
