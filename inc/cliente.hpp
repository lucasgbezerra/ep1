#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include "carrinho.hpp"
#include "produto.hpp"

#include <iostream>
#include <string>
#include <vector>

using namespace  std;

class Cliente

{
    private: 
        string cpf;
        string nome;
        string telefone; 
        string email;
        bool socio;
        vector <string> lista_dados;
        Carrinho carrinho;
    public:
        Cliente();
        ~Cliente();
        Cliente(vector <string> lista_dados);
        Cliente(string cpf, string nome,string telefone,string email);

        string get_cpf();
        void set_cpf(string cpf);
        string get_nome();
        void set_nome(string nome);
        string get_telefone();
        void set_telefone(string telefone);
        string get_email();
        void set_email(string email);
        void set_socio(bool socio);
        bool get_socio();
        Carrinho get_carrinho();

        void imprime_recomendacao();
        void cliente_gera_arquivo();
        void preenche_carrinho();
        virtual void imprime_gastos();
}; 
#endif
