#ifndef SOCIO_HPP
#define SOCIO_HPP
#include "cliente.hpp"
#include "carrinho.hpp"


class Socio : public Cliente
{
private: 
   float desconto;
public:
    Socio();
    Socio(string cpf, string nome,string telefone,string email);
    Socio(vector<string>lista_dados_cliente);
    ~Socio();
    
    void set_desconto();
    float get_desconto();


    void imprime_gastos();
};
#endif
