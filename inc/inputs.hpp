#ifndef INPUTS_HPP
#define INPUTS_HPP

#include <string>
#include <iostream>

using namespace std;


string get_string();
float get_float();
int get_int();
string valida_input();
string valida_cpf();
string valida_nome();
string valida_email();
string valida_telefone();


#endif