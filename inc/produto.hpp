#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <string>
#include <iostream>

using namespace std; 

class Produto
{
    private:
        int codigo;
        string nome_produto;
        float preco;
        string categoria;
        int quantidade;
    public:
        Produto();
        Produto(int codigo,string nome_produto,int quantidade,float preco,string categoria);
        ~Produto();
        
        int get_codigo();
        void set_codigo(int codigo);
        
        string get_nome_produto();
        void set_nome_produto(string get_nome_produto);
        
        float get_preco();
        void set_preco(float preco);

        string get_categoria();
        void set_categoria(string categoria);
        
        int get_quantidade();
        void set_quantidade(int quantidade);

        string verifica_codigo(int codigo);
        Produto * arquivo_gera_produto(int codigo);
        int recebe_codigo();


};

#endif
