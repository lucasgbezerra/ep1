#ifndef CARRINHO_HPP
#define CARRINHO_HPP


#include "produto.hpp"

#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Carrinho
{
private:
    vector<int>quantidade;
    vector <Produto *> lista_produtos;
public:

    Carrinho();
    ~Carrinho();
    
    void set_produto(Produto * lista_produtos);
    vector <Produto *> get_produto();
    void set_quantidade(int quantidade);
    vector<int> get_quantidade();

    

    void adicionar_produto(int codigo,int quantidade);
    bool imprime_carrinho();
    float calcula_preco(float desconto);
    bool compara_quantidades();
    void finalizar_compra(string cpf);
    void registrar_recomendacao(string cpf);
};


#endif