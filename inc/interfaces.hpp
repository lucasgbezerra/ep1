#ifndef INTERFACES_HPP
#define INTERFACES_HPP


#include <vector>
#include <string>
#include <iostream>

class Interfaces
{
public:
    Interfaces();
    ~Interfaces();

    
    void menu_pricipal();
    void modo_venda();
    void modo_venda_cadastro();
    void modo_recomendacao();
    void modo_estoque_menu();
    void escolha_produto();
    void imprime_estoque();
    void cadastrar_produto();
    void adicionar_categoria();
    void adicionar_quantidade();
    void imprimir_informacao_produto();

};
#endif